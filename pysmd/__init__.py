"""
    Created on Jul 22, 2012
"""
import json
from pysmd import get_smd

def get_smd_json(service, pretty=False, **kw):
    """
        Get JSON encoded SMD for 'service'.
        Function exists primarily for demonstration purposes.
        
        @param service: object whose API SMD will describe. 
        @param pretty: if provided and True, return formatted JSON.
        
    """
    if pretty:
        # Don't modify mutable parameters.  It's 
        # just a matter of principle in this case.
        kw = kw.copy()
        kw.setdefault("indent", 2)
    return json.dumps(get_smd(service), **kw)

