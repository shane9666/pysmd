'''
Created on Jul 22, 2012

@author: shane
'''
import unittest
from StringIO import StringIO
from pysmd import get_smd

class Test(unittest.TestCase):
    def setUp(self):
        self.smd = [
            {'name': 'close', 'parameters': []}, 
            {'name': 'flush', 'parameters': []}, 
            {'name': 'getvalue', 'parameters': []}, 
            {'name': 'isatty', 'parameters': []}, 
            {'name': 'next', 'parameters': []}, 
            {'name': 'read', 'parameters': [{'default': -1, 'optional': True, 'name': 'n'}]}, 
            {'name': 'readline', 
             'parameters': [{'default': None, 
                             'optional': True, 
                             'name': 'length'}]},  
            {'name': 'readlines', 
             'parameters': [{'default': 0, 
                             'optional': True, 
                             'name': 'sizehint'}]}, 
            {'name': 'seek', 
             'parameters': [{'name': 'pos'}, 
                            {'default': 0, 
                             'optional': True, 
                             'name': 'mode'}]}, 
            {'name': 'tell', 'parameters': []}, 
            {'name': 'truncate', 
             'parameters': [{'default': None, 
                             'optional': True, 
                             'name': 'size'}]}, 
            {'name': 'write', 'parameters': [{'name': 's'}]}, 
            {'name': 'writelines', 'parameters': [{'name': 'iterable'}]}
        ]
        self.sio = StringIO("this is a long string")
    def tearDown(self):
        self.sio.close()
        self.sio = None
        self.smd = None
    def test_object_smd(self):
        self.assertEqual(self.smd, get_smd(self.sio))
    def test_class_smd(self):
        self.assertEqual(self.smd, get_smd(self.sio.__class__))
    def test_private_smd(self):
        class SIO(StringIO):
            def _fake_method(self): return
        sio = SIO(self.sio.getvalue())
        smd = get_smd(sio, False)
        methods = set([descriptor["name"] for descriptor in smd])
        self.assertIn("__init__", methods, 
                      "Private method '__init__' missing.")
        self.assertIn("__iter__", methods, 
                      "Private method '__iter__' missing")
        self.assertIn("_fake_method", methods, 
                      "Private method '_fake_method' missing.")

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()